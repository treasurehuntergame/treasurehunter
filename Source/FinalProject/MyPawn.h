// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "GameFramework/NavMovementComponent.h"
#include "FinalProject/STypes.h"
#include "Components/AudioComponent.h"
#include "Components/BoxComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "SZombieAIController.h"
#include "MyPawn.generated.h"



UCLASS()
class AMyPawn : public ACharacter
{
	GENERATED_BODY()
	

		/* Last time the player was spotted */
		float LastSeenTime;

	/* Last time the player was heard */
	float LastHeardTime;

	/* Last time we attacked something */
	float LastMeleeAttackTime;

	/* Time-out value to clear the sensed position of the player. Should be higher than Sense interval in the PawnSense component not never miss sense ticks. */
	UPROPERTY(EditDefaultsOnly, Category = "AI")
		float SenseTimeOut;

	/* Resets after sense time-out to avoid unnecessary clearing of target each tick */
	bool bSensedTarget;

	UPROPERTY(EditAnywhere)
		class UPawnSensingComponent* PawnSensingComp;
	
	/** Pawn mesh: 1st person view (arms; seen only by self)
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class  USkeletalMeshComponent* SkeletalMesh; */
public:
	// Sets default values for this pawn's properties
	AMyPawn();


	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void SetGamePaused(bool bIsPaused);

	UPROPERTY(EditAnywhere)
		AActor* MyCharacter;

	UPROPERTY(EditAnywhere)
		FHitResult MyHit;


	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser);

	/** Update Health */
	UFUNCTION(BlueprintCallable, Category = "Health")
		void UpdateHealth(float HealthChange);


	UPROPERTY(EditAnywhere)
		FHitResult MyHitBody;



	UPROPERTY(EditAnywhere)
		TSubclassOf<UDamageType> FireDamageType;

	// declare overlap begin function
	class UBoxComponent* Box;

	// create trigger capsule
	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
		class UCapsuleComponent* TriggerCapsule;


	// create trigger capsule
	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
		class UCapsuleComponent* TriggerCapsuleBody;


	
	UFUNCTION()
	void OnSeePlayer(APawn * Pawn);
	UFUNCTION()
	void OnHearNoise(APawn * PawnInstigator, const FVector & Location, float Volume);

	void Death();

	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	UParticleSystem* ParticleFX1;
	UPROPERTY(EditAnywhere)
		AActor* MyActor;

	UPROPERTY(EditAnywhere)
		AActor* MyActorBody;

	FTimerHandle FireTimerHandle;
	
	bool bCanApplyDamage;


		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float FullHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float HealthPercentage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float PreviousHealth;

	FTimerHandle FireTimerHandle2;
	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class UAnimationAsset* ZombieReactionHit;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class UAnimationAsset* BodyReactionHit;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class UAnimationAsset* ZombieDead;


	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class UAnimationAsset* ZombieAttack;

	/* The bot behavior we want this bot to execute, (passive/patrol) by specifying EditAnywhere we can edit this value per-instance when placed on the map. */
	UPROPERTY(EditAnywhere, Category = "AI")
		EBotBehaviorType BotType;

	/* The thinking part of the brain, steers our zombie and makes decisions based on the data we feed it from the Blackboard */
	/* Assigned at the Character level (instead of Controller) so we may use different zombie behaviors while re-using one controller. */
	UPROPERTY(EditDefaultsOnly, Category = "AI")
		class UBehaviorTree* BehaviorTree;


	AActor* pawn;

	bool IsDied;
	bool IsMoving;
	bool IsPLayingAnim;
	FVector vel;
	FVector zero;
	FVector Loc;
	FRotator Rot;
};
