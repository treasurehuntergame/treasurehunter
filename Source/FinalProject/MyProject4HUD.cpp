// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MyProject4HUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/LevelStreaming.h"
#include "Engine/EngineTypes.h"
#include "Blueprint/UserWidget.h"


AMyProject4HUD::AMyProject4HUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;

	// locate the health hud widget
	static ConstructorHelpers::FClassFinder<UUserWidget> HealthBarObj(TEXT("/Game/UI/HUD/Health_Hud"));
	HUDWidgetClass = HealthBarObj.Class;


}



void AMyProject4HUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition((Center.X),
		(Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);
}

void AMyProject4HUD::BeginPlay()
{
	Super::BeginPlay();


		
		//-------------Create Health Progress Bar Hud----------------	
		// use an if statement to check if HUDWidgetClass exists and is not a null pointer
		if (HUDWidgetClass != nullptr)
		{
			// create the HUDWidgetClass widget
			CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);

			// use an if statement to check if HUDWidgetClass was successfully created
			if (CurrentWidget)
			{
				// add the creatyed widget to the viewport
				CurrentWidget->AddToViewport();
			}
		}
	
	
	}
























