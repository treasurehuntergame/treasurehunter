// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "FinalProject/STypes.h"

#include "SZombieAIController.generated.h"

class UBehaviorTreeComponent;
class ACharacter;

/**
 * 
 */
UCLASS()
class ASZombieAIController : public AAIController
{
	GENERATED_BODY()

	ASZombieAIController();

	/* Called whenever the controller possesses a character bot */
	virtual void OnPossess(class APawn* InPawn) override;

	virtual void OnUnPossess() override;

	UBehaviorTreeComponent* BehaviorComp;

	UBlackboardComponent* BlackboardComp;

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	FName TargetEnemyKeyName;



public:


	

	UFUNCTION()
	void SetTargetEnemy(APawn* NewTarget);
	

	/** Returns BehaviorComp subobject **/
	FORCEINLINE UBehaviorTreeComponent* GetBehaviorComp() const { return BehaviorComp; }

	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }
};
