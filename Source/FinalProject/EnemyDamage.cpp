// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyDamage.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"
#include "FinalProjectCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Engine/World.h"
// Sets default values
AEnemyDamage::AEnemyDamage()
{
	MyBoxComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule Body"));
	MyBoxComponent->InitCapsuleSize(1.0f, 1.0f);;


	Fire = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("My Fire"));
	Fire->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	Fire->SetupAttachment(RootComponent);

	MyBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AEnemyDamage::OnOverlapBegin);
	MyBoxComponent->OnComponentEndOverlap.AddDynamic(this, &AEnemyDamage::OnOverlapEnd);

	bCanApplyDamage = false;
}

void AEnemyDamage::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{

		bCanApplyDamage = true;
		MyCharacter = Cast<AActor>(OtherActor);
		MyHit = SweepResult;
		GetWorldTimerManager().SetTimer(FireTimerHandle, this, &AEnemyDamage::ApplyFireDamage, 2.2f, true, 0.0f);
	}
}

void AEnemyDamage::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	bCanApplyDamage = false;
	GetWorldTimerManager().ClearTimer(FireTimerHandle);
}

void AEnemyDamage::ApplyFireDamage()
{


	if (bCanApplyDamage)
	{
		UGameplayStatics::ApplyPointDamage(MyCharacter, 10.0f, GetActorLocation(), MyHit, nullptr, this, FireDamageType);
	}
}