// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.


#include "SZombieAIController.h"
#include "MyPawn.h"
#include "FinalProjectCharacter.h"


/* AI Specific includes */
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"


ASZombieAIController::ASZombieAIController()
{
	// create the behavior tree component "BehaviorComp"
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));

	// create the behavior tree component "BlackboardComp"
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));

	// Match with the AI/ZombieBlackboard
	TargetEnemyKeyName = "TargetEnemy";

	/* Initializes PlayerState so we can assign a team index to AI */
	bWantsPlayerState = true;
}


void ASZombieAIController::OnPossess(class APawn* InPawn)
{
	Super::OnPossess(InPawn);

	//create a pointer to MyPawn class
	AMyPawn* ZombieBot = Cast<AMyPawn>(InPawn);

	// use an if statement to check if ZombieBot is not a null pointer
	if (ZombieBot)
	{
		if (ensure(ZombieBot->BehaviorTree->BlackboardAsset))
		{
			// Setup Blackboard component for using given blackboard asset
			BlackboardComp->InitializeBlackboard(*ZombieBot->BehaviorTree->BlackboardAsset);
		}
		// start execution of behavior tree using the Blackboard component
		BehaviorComp->StartTree(*ZombieBot->BehaviorTree);
	}
}


void ASZombieAIController::OnUnPossess()
{
	Super::OnUnPossess();

	//Stop any behavior tree from running after unpossesing the pawn
	BehaviorComp->StopTree();
}





void ASZombieAIController::SetTargetEnemy(APawn* NewTarget)
{   
	// use an if statement to check if the Blackboard component exists
	if (BlackboardComp)
	{
		// set the Blackboard component to the new target
		BlackboardComp->SetValueAsObject(TargetEnemyKeyName, NewTarget);
	}
}


