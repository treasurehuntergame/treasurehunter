// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FinalProjectCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "TimerManager.h"
#include "GameFramework/SpringArmComponent.h"
#include "MyPawn.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "EnemyDamage.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Controller.h"
#include "Engine/World.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/EngineTypes.h"
#include "Blueprint/UserWidget.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ATheFightCharacter

AFinalProjectCharacter::AFinalProjectCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	zero = { 0, 0, 0 };
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;


	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)




	//
	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;


	// declare trigger capsule
	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(55.f, 96.0f);;
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(GetMesh());


	// declare trigger capsule
	TriggerCapsuleBody = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule Body"));
	TriggerCapsuleBody->InitCapsuleSize(55.f, 96.0f);;
	TriggerCapsuleBody->SetCollisionProfileName(TEXT("Trigger Body"));
	TriggerCapsuleBody->SetupAttachment(GetMesh());

	// declare overlap events
	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &AFinalProjectCharacter::OnOverlapBegin);


	// locate the health hud widget
	static ConstructorHelpers::FClassFinder<UUserWidget> HealthBarObj(TEXT("/Game/UI/HUD/Health_Hud"));
	HUDWidgetClass = HealthBarObj.Class;


}

void AFinalProjectCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();





	// initialize FullHealth variable
	FullHealth = 1000.0f;
	// initialize Health variable
	Health = FullHealth;
	// initialize HealthPercentage variable
	HealthPercentage = 1.0f;
	// initialize PreviousHealth variable
	PreviousHealth = HealthPercentage;
	// initialize bCanBeDamaged variable
	bCanBeDamaged = true;

	attack1 = false;

	//if (Health > 0 && GetMesh()->IsPlaying() == false) {
		// play the zombie dead animation when the zombie character loses all his health 
	//	GetMesh()->PlayAnimation(idle, false);
	//}

		//-------------Create Health Progress Bar Hud----------------	
		// use an if statement to check if HUDWidgetClass exists and is not a null pointer
	if (HUDWidgetClass != nullptr)
	{
		// create the HUDWidgetClass widget
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);

		// use an if statement to check if HUDWidgetClass was successfully created
		if (CurrentWidget)
		{
			// add the creatyed widget to the viewport
			CurrentWidget->AddToViewport();
		}
	}

}

void AFinalProjectCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// the Fvector type variable retrieves and stores the zombie character's velocity vector
	vel = GetCharacterMovement()->Velocity;

	
	//if (GetMesh()->IsPlaying() == false && attack1 == true) {
	//	attack1 = false;
	//}
	//if (vel == zero) {
	//	Idle();
	//}
	//if (Health <= 0 && GetMesh()->IsPlaying() == false) {
		// call the SetGamePaused function to pause the game
//		SetGamePaused(1);
//	}
}







//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void ATheFightCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}


void AFinalProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Punch", IE_Pressed, this, &AFinalProjectCharacter::punch);
	PlayerInputComponent->BindAction("kick", IE_Pressed, this, &AFinalProjectCharacter::kick);
	PlayerInputComponent->BindAction("RoundKick", IE_Pressed, this, &AFinalProjectCharacter::roundkick);
	PlayerInputComponent->BindAction("BlockAnim", IE_Pressed, this, &AFinalProjectCharacter::block);


	PlayerInputComponent->BindAxis("MoveForward", this, &AFinalProjectCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFinalProjectCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFinalProjectCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFinalProjectCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AFinalProjectCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AFinalProjectCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AFinalProjectCharacter::OnResetVR);
}


void AFinalProjectCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AFinalProjectCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AFinalProjectCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AFinalProjectCharacter::TurnAtRate(float Rate)
{
	if (Health > 0)
	{
		// calculate delta for this frame from the rate information
		AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
	}
}

void AFinalProjectCharacter::LookUpAtRate(float Rate)
{
	if (Health > 0)
	{
		// calculate delta for this frame from the rate information
		AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
	}
}

void AFinalProjectCharacter::Idle()
{
		if (Health > 0.0 && GetMesh()->IsPlaying() == false) {
			// play the zombie dead animation when the zombie character loses all his health 
			GetMesh()->PlayAnimation(idle, false);
		}
}

void AFinalProjectCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);

		/*
		if (Health > 0.0 && GetMesh()->IsPlaying() == false) {
			// play the zombie dead animation when the zombie character loses all his health
			GetMesh()->PlayAnimation(walk, false);
		}
		*/
	}
}

void AFinalProjectCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);

		/*
		if (Health > 0.0 && GetMesh()->IsPlaying() == false) {
			// play the zombie dead animation when the zombie character loses all his health 
			GetMesh()->PlayAnimation(walk, false);
		}
		*/
	}
}


void AFinalProjectCharacter::SetGamePaused(bool bIsPaused)
{
	// get an instance of the player controller
	APlayerController* const MyPlayer = Cast<APlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));

	// check if the player controller variable is not a null pointer
	if (MyPlayer != NULL)
	{
		//  pause the game play screen
		MyPlayer->SetPause(bIsPaused);
	}


}


void AFinalProjectCharacter::punch()
{
	// check if the ZombieReactionHit animation is not a null pointer, and if the zombie character's health is not zero
	if (punchanim != NULL && Health > 0.0 && GetMesh()->IsPlaying() == false)
	{

		//if(GetMesh()->IsPlaying() == false){
		//attack1 = true;
		//}
	}
}

void AFinalProjectCharacter::kick()
{
	// check if the ZombieReactionHit animation is not a null pointer, and if the zombie character's health is not zero
	if (kickanim != NULL && Health > 0.0 && GetMesh()->IsPlaying() == false)
	{
		
		// play the zombie hit animation when the zombie character is hit  
	//	GetMesh()->PlayAnimation(kickanim, false);

	}
}


void AFinalProjectCharacter::roundkick()
{
	// check if the ZombieReactionHit animation is not a null pointer, and if the zombie character's health is not zero
	if (roundkickanim != NULL && Health > 0.0 && GetMesh()->IsPlaying() == false)
	{

		// play the zombie hit animation when the zombie character is hit  
	//	GetMesh()->PlayAnimation(roundkickanim, false);

	}
}

void AFinalProjectCharacter::block()
{
	// check if the ZombieReactionHit animation is not a null pointer, and if the zombie character's health is not zero
	if (punchanim != NULL && Health > 0.0 && GetMesh()->IsPlaying() == false)
	{
	
		// play the zombie hit animation when the zombie character is hit  
		//GetMesh()->PlayAnimation(blockanim, false);
	
	}
}

bool AFinalProjectCharacter::IsAlive() const
{

	return Health > -1.0;
}

void AFinalProjectCharacter::death()
{
	// play the zombie dead animation when the zombie character loses all his health 
	//GetMesh()->PlayAnimation(ZombieDead, false);
}


float AFinalProjectCharacter::GetHealth()
{
	// return the health percentage as a float which is used by the health progress bar to display the remaining health
	return HealthPercentage;
}


FText AFinalProjectCharacter::GetHealthIntText()
{
	// convert the health percentage from float to the nearest integer
	int32 HP = FMath::RoundHalfFromZero(HealthPercentage * 100);

	// convert integer value of HP to a string value
	FString HPS = FString::FromInt(HP);

	// add the % sign to the health text
	FString HealthHUD = HPS + FString(TEXT("%"));

	//  Generate an FText representing the passed in string
	FText HPText = FText::FromString(HealthHUD);

	// return the current health as a text which is used by the health text to display the remaining health  percentage
	return HPText;
}







void AFinalProjectCharacter::UpdateHealth(float HealthChange)
{
	// calculate the remainig health value by adding the negated damage value
	Health += HealthChange;
	// limit or clamp the health value between zero and the full health
	Health = FMath::Clamp(Health, 0.0f, FullHealth);

	// calculate health percentage
	HealthPercentage = Health / FullHealth;
}




float AFinalProjectCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser)
{
	//set variable to false
	bCanBeDamaged = false;
	if (Cast<AEnemyDamage>(DamageCauser) != nullptr)
	{
		//call UpdateHealth to subtract the damage points
		UpdateHealth(-DamageAmount);
	}
	//return the damage amount taken by the player as a float
	return DamageAmount;
}



void AFinalProjectCharacter::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// check if the zombie character is hit by the projectile and not something else
	if (Cast<AMyPawn>(OtherActor) != nullptr)
	{

		UpdateHealth(-20.0);
	}
}

