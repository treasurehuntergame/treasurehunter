// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPawn.h"
#include "FinalProjectCharacter.h"

#include "Animation/AnimInstance.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Engine/World.h"
#include "CampFire.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/EngineTypes.h"
#include "Engine/World.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
/* AI Include */
#include "Perception/PawnSensingComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/NavMovementComponent.h"
#include "Components/AudioComponent.h"
#include "Components/CapsuleComponent.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "Components/PointLightComponent.h"
#include "Sound/SoundCue.h"

// Sets default values
AMyPawn::AMyPawn()
{
	
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// initialize the IsDied variable to false
	IsDied = false;
	// initialize the zero vector to {0,0,0}
	zero = { 0, 0, 0 };
	
	/*create a sensing component to detect players by visibility and noise. */
	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
	PawnSensingComp->SetPeripheralVisionAngle(60.0f);
	PawnSensingComp->SightRadius = 2000;
	PawnSensingComp->HearingThreshold = 600;
	PawnSensingComp->LOSHearingThreshold = 1200;

	/* By default we will not let the AI patrol, we can override this value per-instance. */
	BotType = EBotBehaviorType::Patrolling;
	SenseTimeOut = 2.5f;

	// declare trigger capsule
	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(55.f, 96.0f);;
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);


	// declare trigger capsule
	TriggerCapsuleBody = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule Body"));
	TriggerCapsuleBody->InitCapsuleSize(55.f, 96.0f);;
	TriggerCapsuleBody->SetCollisionProfileName(TEXT("Trigger Body"));
	TriggerCapsuleBody->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();

	// initialize the zombie character's health to 100
	Health = 1000;
	
	// use an if statement to bind our delegates to the component when PawnSensingComp is true 
	if (PawnSensingComp)
	{
		PawnSensingComp->OnSeePawn.AddDynamic(this, &AMyPawn::OnSeePlayer);
		PawnSensingComp->OnHearNoise.AddDynamic(this, &AMyPawn::OnHearNoise);
	}
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	// The boolean variable is used to store the true or false state of whether an animation associated with the zombie character's mesh is currently playing
	


	// the Fvector type variable retrieves and stores the zombie character's velocity vector
	vel = GetCharacterMovement()->Velocity;

	// use an if statement to Check if the last time we sensed a player is beyond the time out value to prevent bot from endlessly following a player.
	//if (bSensedTarget && (GetWorld()->TimeSeconds - LastSeenTime) > SenseTimeOut
	//	&& (GetWorld()->TimeSeconds - LastHeardTime) > SenseTimeOut)
	//{
	//	ASZombieAIController* AIController = Cast<ASZombieAIController>(GetController());
	//	if (AIController)
	//	{
	//		bSensedTarget = false;
			/* Reset */
	//		AIController->SetTargetEnemy(nullptr);
	//	}
	//}
}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}



void AMyPawn::SetGamePaused(bool bIsPaused)
{
	// get an instance of the player controller
	APlayerController* const MyPlayer = Cast<APlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));

	// check if the player controller variable is not a null pointer
	if (MyPlayer != NULL)
	{
		//  pause the game play screen
		MyPlayer->SetPause(bIsPaused);
	}


}



void AMyPawn::UpdateHealth(float HealthChange)
{
	// calculate the remainig health value by adding the negated damage value
	Health += HealthChange;

}




float AMyPawn::TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser)
{
	//set variable to false
	bCanBeDamaged = false;

	if (Cast<ACampFire>(DamageCauser) != nullptr)
	{
		if (Health == 0.0) {
			GetMesh()->PlayAnimation(ZombieDead, false);
			
		}
		if(Health > 0.0){
		//call UpdateHealth to subtract the damage points
		UpdateHealth(-DamageAmount);
		}
	
	}

	//return the damage amount taken by the player as a float
	return DamageAmount;
}


void  AMyPawn::OnSeePlayer(APawn* Pawn)
{

	// Keep track of the time the player was last sensed in order to clear the target 
	LastSeenTime = GetWorld()->GetTimeSeconds();
	// set bSensedTarget to true
	bSensedTarget = true;

	//create a pointer to ASZombieAIController class
	ASZombieAIController* AIController = Cast<ASZombieAIController>(GetController());

	//create a pointer to AMyProject4Character class
	AFinalProjectCharacter* SensedPawn = Cast<AFinalProjectCharacter>(Pawn);

	// use an if statement to check if AIController pointer is not null and if the player character is not dead
	if (AIController && SensedPawn->IsAlive() && Health > 0.0)
	{

	
		// set the target enemy in the AIController to the sensed player character
		AIController->SetTargetEnemy(SensedPawn);

	}


	if (Health <= 0 || (SensedPawn->IsAlive() == false)) {
		ASZombieAIController* AIController = Cast<ASZombieAIController>(GetController());
		if (AIController)
		{
			bSensedTarget = false;
			/* Reset */
			AIController->SetTargetEnemy(nullptr);
		}
	}

	
	
}


void AMyPawn::OnHearNoise(APawn* PawnInstigator, const FVector& Location, float Volume)
{
	// set bSensedTarget to true
	bSensedTarget = true;
	// Keep track of the time the player was last sensed in order to clear the target 
	LastHeardTime = GetWorld()->GetTimeSeconds();

	//create a pointer to ASZombieAIController class
	ASZombieAIController* AIController = Cast<ASZombieAIController>(GetController());

	// use an if statement to check if AIController pointer is not null
	if (AIController)
	{
		// set the target enemy in the AIController to the sensed player character
		AIController->SetTargetEnemy(PawnInstigator);
	}
}


void AMyPawn::Death()
{
	// play the zombie dead animation when the zombie character loses all his health 
	

	if (Health == 0.0) {
		GetMesh()->PlayAnimation(ZombieDead, false);
		//Death();
		// call the SetGamePaused function to pause the game
		//GetWorldTimerManager().SetTimer(FireTimerHandle2, this, &AMyPawn::Death, 10.2f, true, 10.0f);
		Destroy();
	}
	
}





